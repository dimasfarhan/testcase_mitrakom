<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Traits\AuditTrails;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use DataTables;

class ProjectController extends Controller
{
    use AuditTrails;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('layouts.projects.index');
    }

    public function getProjects(Request $request) {
        if ($request->ajax()) {
            $data = Project::latest()->where('deleted_at', null)->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('projects.updateView', [$row->id]).'" class="edit btn btn-warning btn-sm text-light">Edit</a>
                                  <form action="'.route('projects.delete', [$row->id]).'" method="POST" style="display: contents;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="delete btn btn-danger btn-sm" title="Delete" >Delete</button>
                                  </form>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function createView() {
        return view('layouts.projects.add');
    }

    public function create(Request $request) {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'start_at' => ['required', 'date', 'after:today'],
            'finish_at' => ['required', 'date', 'after_or_equal:start_at']
        ]);

        $project = Project::create([
            'name' => $data['name'],
            'description' => $data['description'],
            'start_at' => $data['start_at'],
            'finish_at' => $data['finish_at']
        ]);

        $project = $this->setCreatedAuditTrails($project);
        $project->save();

        return redirect()->route('projects.list')->with('success', 'Success adding new project.');
    }

    public function updateView($id) {
        $project = Project::findOrFail($id);
        return view('layouts.projects.edit', compact('project'));
    }

    public function update(Request $request, $id) {
        $project = project::findOrFail($id);

        $data = $request->validate([
            'name' => ['string', 'max:255'],
            'description' => ['string'],
            'start_at' => ['nullable', 'date', 'after:today'],
            'finish_at' => ['nullable', 'date', 'after_or_equal:start_at']
        ]);

        $project->name = $data['name'] === null || $data['name'] === '' ? $project->name : $data['name'];
        $project->description = $data['description'] === null || $data['description'] === '' ? $project->description : $data['description'];
        $project->start_at = $data['start_at'] === null || $data['start_at'] === '' ? $project->start_at : $data['start_at'];
        $project->finish_at = $data['finish_at'] === null || $data['finish_at'] === '' ? $project->finish_at : $data['finish_at'];
        $project = $this->setUpdatedAuditTrails($project);

        $project->save();

        return redirect()->route('projects.list')->with('success', 'Success edit existing project.');
    }

    public function delete($id) {
        $project = Project::findOrFail($id);

        $project = $this->setDeletedAuditTrails($project);
        $project->save();
        return redirect()->route('projects.list')->with('success', 'Success deleting project '.$project->name.'.');
    }
}
