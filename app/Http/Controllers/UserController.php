<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\AuditTrails;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use DataTables;

class UserController extends Controller
{
    use AuditTrails;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('layouts.users.index');
    }

    public function getUsers(Request $request) {
        if ($request->ajax()) {
            $data = User::latest()->where('deleted_at', null)->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('users.updateView', [$row->id]).'" class="edit btn btn-warning btn-sm text-light">Edit</a>
                                  <form action="'.route('users.delete', [$row->id]).'" method="POST" style="display: contents;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="delete btn btn-danger btn-sm" title="Delete" >Delete</button>
                                  </form>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function createView() {
        return view('layouts.users.add');
    }

    public function create(Request $request) {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => ['required'],
        ]);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role' => $data['role']
        ]);

        $user = $this->setCreatedAuditTrails($user);
        $user->save();

        return redirect()->route('users.list')->with('success', 'Success adding new user.');
    }

    public function updateView($id) {
        $user = User::findOrFail($id);
        return view('layouts.users.edit', compact('user'));
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);

        $data = $request->validate([
            'name' => ['string', 'max:255'],
            'email' => ['string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            'role' => ['required'],
        ]);

        $user->name = $data['name'] === null || $data['name'] === '' ? $user->name : $data['name'];
        $user->email = $data['email'] === null || $data['email'] === '' ? $user->email : $data['email'];
        $user->password = $data['password'] === null || $data['password'] === '' ? $user->email : bcrypt($data['password']);
        $user->role = $data['role'];
        $user = $this->setUpdatedAuditTrails($user);

        $user->save();

        return redirect()->route('users.list')->with('success', 'Success edit existing user.');
    }

    public function delete($id) {
        $user = User::findOrFail($id);
        if ($user->email === Auth::user()->email) {
            return redirect()->route('users.list')->withErrors([
                'msg' => 'User is logged in'
            ]);
        }

        $user = $this->setDeletedAuditTrails($user);
        $user->save();
        return redirect()->route('users.list')->with('success', 'Success deleting user '.$user->name.'.');
    }
}
