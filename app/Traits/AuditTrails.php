<?php

namespace App\Traits;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
trait AuditTrails
{
    private $timezone = 'Asia/Jakarta';

    protected function setCreatedAuditTrails($object) {
        $object->created_at = Carbon::now()->setTimezone($this->timezone);
        $object->created_by = Auth::id();
        return $object;
    }

    protected function setUpdatedAuditTrails($object) {
        $object->updated_at = Carbon::now()->setTimezone($this->timezone);
        $object->updated_by = Auth::id();
        return $object;
    }

    protected function setDeletedAuditTrails($object) {
        $object->deleted_at = Carbon::now()->setTimezone($this->timezone);
        $object->deleted_by = Auth::id();
        return $object;
    }
}
