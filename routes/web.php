<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

// USER ROUTE
Route::group(['prefix' => 'users', 'as' => 'users.'], function() {
    Route::get('/', [UserController::class, 'index'])->name('list');
    Route::get('/user-list', [UserController::class, 'getUsers'])->name('fetchdata');

    Route::get('/add', [UserController::class, 'createView'])->name('createView');
    Route::post('/add', [UserController::class, 'create'])->name('create');

    Route::get('/edit/{id}', [UserController::class, 'updateView'])->name('updateView');
    Route::patch('/edit/{id}', [UserController::class, 'update'])->name('update');

    Route::delete('/delete/{id}', [UserController::class, 'delete'])->name('delete');
});

// PROJECTS ROUTE
Route::group(['prefix' => 'projects', 'as' => 'projects.'], function() {
    Route::get('/', [ProjectController::class, 'index'])->name('list');
    Route::get('/project-list', [ProjectController::class, 'getprojects'])->name('fetchdata');

    Route::get('/add', [ProjectController::class, 'createView'])->name('createView');
    Route::post('/add', [ProjectController::class, 'create'])->name('create');

    Route::get('/edit/{id}', [ProjectController::class, 'updateView'])->name('updateView');
    Route::patch('/edit/{id}', [ProjectController::class, 'update'])->name('update');

    Route::delete('/delete/{id}', [ProjectController::class, 'delete'])->name('delete');
});
