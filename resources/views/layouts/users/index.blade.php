@extends('adminlte::page')

@section('title', 'Users')

@section('content_header')
    <h1>List All Users</h1>
@stop

@section('content')
    <x-adminlte-card title="Users Table" theme="dark" icon="fas fa-lg fa-users" collapsible>

        @if (\Session::has('success'))
            <x-adminlte-alert theme="info" title="Success">
                {!! \Session::get('success') !!}
            </x-adminlte-alert>
        @endif

        @error('msg')
            <x-adminlte-alert theme="danger" title="Warning">
                {!! $message !!}
            </x-adminlte-alert>
        @enderror

        <a href="{{ route('users.createView') }}" class="edit btn btn-flat btn-success mb-3">Tambah User</a>

        <table class="table table-bordered yajra-datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

    </x-adminlte-card>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.fetchdata') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'role', name: 'role'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        });
     </script>
@stop
