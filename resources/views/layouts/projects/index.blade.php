@extends('adminlte::page')

@section('title', 'Projects')

@section('content_header')
    <h1>List All Projects</h1>
@stop

@section('content')
    <x-adminlte-card title="Projects Table" theme="dark" icon="fas fa-lg fa-suitcase" collapsible>

        @if (\Session::has('success'))
            <x-adminlte-alert theme="info" title="Success">
                {!! \Session::get('success') !!}
            </x-adminlte-alert>
        @endif

        @error('msg')
            <x-adminlte-alert theme="danger" title="Warning">
                {!! $message !!}
            </x-adminlte-alert>
        @enderror

        <a href="{{ route('projects.createView') }}" class="edit btn btn-flat btn-success mb-3">Tambah Project</a>

        <table class="table table-bordered yajra-datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Start Date</th>
                    <th>Finish Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

    </x-adminlte-card>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('projects.fetchdata') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'start_at', name: 'start_at'},
                    {data: 'finish_at', name: 'finish_at'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        });
     </script>
@stop
