@extends('adminlte::page')

@section('title', 'Project')

@section('content_header')
    <h1>Edit Project</h1>
@stop

@section('content')
    <x-adminlte-card title="Form Create Project" theme="dark" icon="fas fa-lg fa-suitcase" collapsible>
        <form action="{{ route('projects.update', $project->id) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}

            <div class="row">
                <div class="col-md-6">
                    {{-- Name field --}}
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                            value="{{ $project->name }}" placeholder="{{ 'Project Name...' }}" autofocus>

                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-suitcase {{ config('adminlte.classes_auth_icon', '') }}"></span>
                            </div>
                        </div>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    {{-- Description field --}}
                    <div class="input-group mb-3">
                        <input type="text" name="description" class="form-control @error('description') is-invalid @enderror"
                            value="{{ $project->description }}" placeholder="{{ 'Project Description...' }}">

                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-file-alt {{ config('adminlte.classes_auth_icon', '') }}"></span>
                            </div>
                        </div>

                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    {{-- Start date field --}}
                    <div class="input-group mb-3">
                        <input type="date" name="start_at" class="form-control @error('start_at') is-invalid @enderror"
                        value="{{ $project->start_at }}">

                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-calendar {{ config('adminlte.classes_auth_icon', '') }}"></span>
                            </div>
                        </div>

                        @error('start_at')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    {{-- Finish date field --}}
                    <div class="input-group mb-3">
                        <input type="date" name="finish_at"
                            class="form-control @error('finish_at') is-invalid @enderror" value="{{ $project->finish_at }}">

                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-calendar {{ config('adminlte.classes_auth_icon', '') }}"></span>
                            </div>
                        </div>

                        @error('finish_at')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            {{-- Register button --}}
            <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                <span class="fas fa-user-plus"></span>
                {{ "Submit" }}
            </button>

        </form>

    </x-adminlte-card>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
     </script>
@stop
